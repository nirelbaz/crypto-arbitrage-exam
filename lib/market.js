const _ = require('lodash')
const WebSocket = require('ws')
const EventEmitter = require('events')
const Pair = require('./pair')

module.exports = class Market extends EventEmitter {
  constructor () {
    super()

    this.pairs = {}

    this.listenToTickSocket()
  }

  listenToTickSocket () {
    const ws = new WebSocket('wss://stream.binance.com:9443/ws/!ticker@arr')
    ws.on('message', message => this.handleMessage(message))
  }

  handleMessage (message) {
    let data = JSON.parse(message)
    data.forEach(tick => this.updatePair(tick.s, tick))
    this.broadcastMarketUpdate()
  }

  updatePair (symbol, streamData) {
    if (this.pairs[symbol]) {
      this.pairs[symbol].update(streamData)
    } else {
      this.pairs[symbol] = new Pair(streamData)
    }
  }

  broadcastMarketUpdate () {
    this.emit('market-update', Object.values(this.pairs))
  }
}
