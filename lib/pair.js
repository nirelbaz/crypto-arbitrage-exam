const EventEmitter = require('events')
const _ = require('lodash')

module.exports = class Pair extends EventEmitter {
  constructor (streamData) {
    super()
    this.currencies = ['USDT', 'BTC', 'ETH', 'BNB']
    this.setMaxListeners(1000)
    this.update(streamData)
  }

  update (streamData) {
    this.symbol = streamData.s
    this.currency = this.getCurrencyFromSymbol(streamData.s)
    this.coin = this.getCoinFromSymbol(streamData.s)
    this.price = Number(streamData.c)

    this.emit('pair-update', this)
  }

  getCurrencyFromSymbol (symbol) {
    return _.findLast(this.currencies, currency => _.endsWith(symbol, currency))
  }

  getCoinFromSymbol (symbol) {
    return _.replace(symbol, new RegExp(`(${this.currencies.join('|')})$`, 'i'), '')
  }
}
