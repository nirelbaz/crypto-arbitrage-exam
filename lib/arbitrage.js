const WebSocket = require('ws')
const Market = require('./market')

module.exports = class Arbitrage {
  constructor (wss) {
    this.wss = wss
    this.market = new Market()

    this.market.on('market-update', msg => this.broadcast('market', msg))
  }

  broadcast (type, msg) {
    this.wss.clients.forEach(client => {
      if (client.readyState === WebSocket.OPEN) {
        client.send(JSON.stringify({[type]: msg}))
      }
    })
  }
}
