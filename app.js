const Arbitrage = require('./lib/arbitrage')
const Koa = require('koa')
const views = require('koa-views')
const WebSocket = require('ws')

const koa = new Koa()
const wss = new WebSocket.Server({ port: 8080 })
const arbitrage = new Arbitrage(wss)

koa.use(views(`${__dirname}/client`))
koa.use(ctx => ctx.render('index'))
koa.listen(8000)
